package br.com.ioasysteste.utils;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.ioasysteste.communications.RetrofitConfig;

public class Preferences {
    Context context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public Preferences(Context context) {
        this.context = context;
    }

    public String getToken() {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        return pref.getString(RetrofitConfig.token, "");
    }

    public void setToken(String token) {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(RetrofitConfig.token, token);
        editor.commit();
    }

    public String getUid() {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        return pref.getString(RetrofitConfig.uid, "");
    }

    public void setUid(String uid) {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(RetrofitConfig.uid, uid);
        editor.commit();
    }

    public String getClient() {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        return pref.getString(RetrofitConfig.client, "");
    }

    public void setClient(String client) {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(RetrofitConfig.client, client);
        editor.commit();
    }

    public void logout() {
        pref = context.getSharedPreferences(RetrofitConfig.preferencias, context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(RetrofitConfig.token, "");
        editor.putString(RetrofitConfig.uid, "");
        editor.putString(RetrofitConfig.client, "");
        editor.commit();
    }


}
