package br.com.ioasysteste.model;

import com.google.gson.annotations.SerializedName;

public class Investor {
    @SerializedName("id")
    int id;

    @SerializedName("investor_name")
    String investor_name;

    @SerializedName("email")
    String email;

    @SerializedName("city")
    String city;

    @SerializedName("country")
    String country;

    @SerializedName("balance")
    float balance;

    @SerializedName("photo")
    String photo;

    @SerializedName("portfolio")
    Portfolio portfolio;

    @SerializedName("portfolio_value")
    float portfolio_value;

    @SerializedName("first_access")
    boolean first_access;

    @SerializedName("super_angel")
    boolean super_angel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public float getPortfolio_value() {
        return portfolio_value;
    }

    public void setPortfolio_value(float portfolio_value) {
        this.portfolio_value = portfolio_value;
    }

    public boolean isFirst_access() {
        return first_access;
    }

    public void setFirst_access(boolean first_access) {
        this.first_access = first_access;
    }

    public boolean isSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(boolean super_angel) {
        this.super_angel = super_angel;
    }
}
