package br.com.ioasysteste.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Portfolio {
    @SerializedName("enterprises_number")
    int enterprises_number;

    @SerializedName("enterprises")
    List<Enterprise> enterprises;
}
