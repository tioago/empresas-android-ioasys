package br.com.ioasysteste.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.ioasysteste.R;
import br.com.ioasysteste.communications.RetrofitConfig;
import br.com.ioasysteste.model.Enterprise;
import br.com.ioasysteste.model.response.ResponseEnterprise;
import br.com.ioasysteste.utils.Preferences;
import br.com.ioasysteste.viewModel.EnterpriseViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterpriseActivity extends AppCompatActivity {

    EnterpriseViewModel enterpriseViewModel;
    Preferences pref;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);

        enterpriseViewModel = new EnterpriseViewModel(this);
        pref = new Preferences(this);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            id = extras.getInt("id");
        }

        getEnterprise(id);

    }


    void getEnterprise(int idEnterprise) {
        Call<ResponseEnterprise> call = new RetrofitConfig().getService().getEnterprise("application/json", pref.getToken(), pref.getClient(), pref.getUid(),id);

        call.enqueue(new Callback<ResponseEnterprise>() {
            @Override
            public void onResponse(Call<ResponseEnterprise> call, Response<ResponseEnterprise> response) {
                Enterprise enterprise = response.body().getEnterprise();
                setTitle(enterprise.getEnterprise_name());
                if (enterprise.getPhoto()!=null && !enterprise.getPhoto().isEmpty()) {
                    uploadImg(enterpriseViewModel.getImgEnterprise(), enterprise.getPhoto());
                }
                enterpriseViewModel.getTextDescription().setText(enterprise.getDescription());
            }

            @Override
            public void onFailure(Call<ResponseEnterprise> call, Throwable t) {

            }
        });
    }


    void uploadImg(ImageView img, String url) {
        Glide.with(this).load(url).into(img);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return true;
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
