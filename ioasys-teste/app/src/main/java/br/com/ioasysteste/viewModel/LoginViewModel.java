package br.com.ioasysteste.viewModel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.ioasysteste.R;
import br.com.ioasysteste.views.LoginActivity;

public class LoginViewModel {
    TextView emailFontAwesome, passwordFontAwesome;
    EditText email, password;
    Button btnEnter;
    Typeface fontAwesome;

    public LoginViewModel(LoginActivity context) {

        email = (EditText) context.findViewById(R.id.email);
        password = (EditText) context.findViewById(R.id.password);
        btnEnter = (Button) context.findViewById(R.id.btnEnter);
        emailFontAwesome = (TextView) context.findViewById(R.id.emailFontAwesome);
        passwordFontAwesome = (TextView) context.findViewById(R.id.passwordFontAwesome);

        fontAwesome = Typeface.createFromAsset(context.getAssets(), "fontawesome.ttf");

        emailFontAwesome.setTypeface(fontAwesome);
        passwordFontAwesome.setTypeface(fontAwesome);

        
    }

    public EditText getEmail() {
        return email;
    }

    public void setEmail(EditText email) {
        this.email = email;
    }

    public EditText getPassword() {
        return password;
    }

    public void setPassword(EditText password) {
        this.password = password;
    }

    public Button getBtnEnter() {
        return btnEnter;
    }

    public void setBtnEnter(Button btnEnter) {
        this.btnEnter = btnEnter;
    }
}
