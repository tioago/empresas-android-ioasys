package br.com.ioasysteste.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.ioasysteste.model.Enterprise;

public class ResponseEnterprises {
    @SerializedName("enterprises")
    List<Enterprise> enterprises;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
