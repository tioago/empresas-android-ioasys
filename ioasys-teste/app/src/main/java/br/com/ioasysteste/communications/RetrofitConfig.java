package br.com.ioasysteste.communications;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {
    private final Retrofit retrofit;

    public static final String preferencias = "preferencias";
    public static final String uid = "uid";
    public static final String client = "client";
    public static final String token = "login";

    public RetrofitConfig() {

        retrofit = new Retrofit.Builder()
                .baseUrl("https://empresas.ioasys.com.br/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ApiService getService() {
        return RetrofitConfig.this.retrofit.create(ApiService.class);
    }

}
