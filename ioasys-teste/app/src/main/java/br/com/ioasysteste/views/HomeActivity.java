package br.com.ioasysteste.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;


import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import br.com.ioasysteste.R;
import br.com.ioasysteste.adapters.ListEnterprisesAdapter;
import br.com.ioasysteste.communications.RetrofitConfig;
import br.com.ioasysteste.model.Enterprise;
import br.com.ioasysteste.model.response.ResponseEnterprises;
import br.com.ioasysteste.utils.DialogHoldon;
import br.com.ioasysteste.utils.Preferences;
import br.com.ioasysteste.viewModel.HomeViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    HomeViewModel home;
    Preferences pref;
    DialogHoldon dialogHoldon;

    List<Enterprise> enterpriseList = new ArrayList<Enterprise>();
    ListEnterprisesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        pref = new Preferences(this);

        home = new HomeViewModel(this);

        dialogHoldon = new DialogHoldon(this);

        setSupportActionBar(home.getToolbar());

        // Get the intent, verify the action and get the query

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.e("TESTE ", query);
            getEnterprises(query);
        }

        adapter = new ListEnterprisesAdapter(this, enterpriseList);
        home.getList().setAdapter(adapter);

        home.getList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(HomeActivity.this,EnterpriseActivity.class);
                intent.putExtra("id",enterpriseList.get(position).getId());
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
    }

    public void getEnterprises(String query) {

        dialogHoldon.setMessage(getString(R.string.holdonListEnterprise));
        dialogHoldon.setCanceledOnTouchOutside(false);
        dialogHoldon.showDialog();
        enterpriseList.clear();

        Call<ResponseEnterprises> call = new RetrofitConfig().getService().getEnterprises("application/json", pref.getToken(), pref.getClient(), pref.getUid(),"1", query);
        call.enqueue(new Callback<ResponseEnterprises>() {
            @Override
            public void onResponse(Call<ResponseEnterprises> call, Response<ResponseEnterprises> response) {
                Log.e("TESTE > ", response.code() + " < CODE");
                dialogHoldon.hideDialog();
                if (response.code()==200) {
                    ResponseEnterprises resp = response.body();
                    if (resp.getEnterprises().size() > 0) {
                        home.getTextExplain().setVisibility(View.GONE);
                        enterpriseList.addAll(resp.getEnterprises());
                        adapter.notifyDataSetChanged();
                    } else {
                        home.getTextExplain().setVisibility(View.VISIBLE);
                    }
                } else {
                    pref.logout();
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onFailure(Call<ResponseEnterprises> call, Throwable t) {
                dialogHoldon.hideDialog();
                Toast.makeText(HomeActivity.this, t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }



}
