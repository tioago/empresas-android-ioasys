package br.com.ioasysteste.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import br.com.ioasysteste.R;
import br.com.ioasysteste.model.Enterprise;
import br.com.ioasysteste.viewModel.ListEnterpriseViewModel;

public class ListEnterprisesAdapter extends BaseAdapter {

    List<Enterprise> enterpriseList = new ArrayList<Enterprise>();
    Context context;

    public ListEnterprisesAdapter(Context context, List<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
        this.context = context;
    }


    @Override
    public int getCount() {
        return enterpriseList.size();
    }

    @Override
    public Object getItem(int position) {
        return enterpriseList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ListEnterpriseViewModel holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.enterprises_list, parent, false);
            holder = new ListEnterpriseViewModel();

            holder.setImgEnterprise((ImageView) convertView.findViewById(R.id.imgEnterprise));
            holder.setTitleEnterprise((TextView) convertView.findViewById(R.id.titleEnterprise));
            holder.setCountryEnterprise((TextView) convertView.findViewById(R.id.countryEnterprise));
            holder.setTypeBusiness((TextView) convertView.findViewById(R.id.typeBusiness));

            convertView.setTag(holder);

        } else {
            holder = (ListEnterpriseViewModel) convertView.getTag();
        }

        Enterprise enterprise = enterpriseList.get(position);

        if (enterprise.getPhoto()!=null && !enterprise.getPhoto().isEmpty()) {
            uploadImg(holder.getImgEnterprise(), enterprise.getPhoto());
        }

        holder.getTitleEnterprise().setText(enterprise.getEnterprise_name());
        holder.getCountryEnterprise().setText(enterprise.getCountry());
        holder.getTypeBusiness().setText(enterprise.getEnterpriseType().getEnterprise_type_name());

        return convertView;
    }

    void uploadImg(ImageView img, String url) {
        Glide.with(context).load(url).into(img);

    }
}
