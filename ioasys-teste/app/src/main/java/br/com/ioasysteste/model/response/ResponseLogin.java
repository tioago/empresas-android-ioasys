package br.com.ioasysteste.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.ioasysteste.model.Enterprise;
import br.com.ioasysteste.model.Investor;

public class ResponseLogin {

    @SerializedName("investor")
    Investor investor;

    @SerializedName("enterprise")
    Enterprise enterprise;

    @SerializedName("success")
    boolean success;

    @SerializedName("errors")
    List<String> errors;


    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
