package br.com.ioasysteste.viewModel;

import android.widget.ImageView;
import android.widget.TextView;

public class ListEnterpriseViewModel {
    ImageView imgEnterprise;
    TextView titleEnterprise, typeBusiness, countryEnterprise;

    public ImageView getImgEnterprise() {
        return imgEnterprise;
    }

    public void setImgEnterprise(ImageView imgEnterprise) {
        this.imgEnterprise = imgEnterprise;
    }

    public TextView getTitleEnterprise() {
        return titleEnterprise;
    }

    public void setTitleEnterprise(TextView titleEnterprise) {
        this.titleEnterprise = titleEnterprise;
    }

    public TextView getTypeBusiness() {
        return typeBusiness;
    }

    public void setTypeBusiness(TextView typeBusiness) {
        this.typeBusiness = typeBusiness;
    }

    public TextView getCountryEnterprise() {
        return countryEnterprise;
    }

    public void setCountryEnterprise(TextView countryEnterprise) {
        this.countryEnterprise = countryEnterprise;
    }
}
