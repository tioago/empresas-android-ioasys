package br.com.ioasysteste.viewModel;

import android.app.Activity;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import br.com.ioasysteste.R;

public class HomeViewModel {
    Toolbar toolbar;
    ListView list;
    TextView textExplain;

    public HomeViewModel(Activity context) {
        toolbar = (Toolbar) context.findViewById(R.id.toolbar);
        textExplain = (TextView) context.findViewById(R.id.textExplain);
        list = (ListView) context.findViewById(R.id.list);

    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public TextView getTextExplain() {
        return textExplain;
    }

    public void setTextExplain(TextView textExplain) {
        this.textExplain = textExplain;
    }

    public ListView getList() {
        return list;
    }

    public void setList(ListView list) {
        this.list = list;
    }
}
