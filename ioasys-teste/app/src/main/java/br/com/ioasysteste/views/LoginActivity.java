package br.com.ioasysteste.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import br.com.ioasysteste.R;
import br.com.ioasysteste.communications.RetrofitConfig;
import br.com.ioasysteste.model.UserLogin;
import br.com.ioasysteste.model.response.ResponseLogin;
import br.com.ioasysteste.utils.CheckOnline;
import br.com.ioasysteste.utils.DialogHoldon;
import br.com.ioasysteste.utils.Preferences;
import br.com.ioasysteste.viewModel.LoginViewModel;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    LoginViewModel login;
    CheckOnline check;
    DialogHoldon dialogHoldon;
    Preferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         login = new LoginViewModel(this);
         check = new CheckOnline(this);
         pref = new Preferences(this);

         dialogHoldon = new DialogHoldon(this);

         dialogHoldon.setMessage(getString(R.string.loginMessage));

         login.getBtnEnter().setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 verificaLogin();
             }
         });

         if (pref.getToken()!=null && !pref.getToken().isEmpty()) {
             openHome();
         }


    }

    private void verificaLogin() {
        if (check.isOnline()) {
            if (login.getEmail().getText().toString() != null && !login.getEmail().getText().toString().isEmpty()) {
                if (login.getPassword().getText().toString() != null && !login.getPassword().getText().toString().isEmpty()) {
                    signIn(login.getEmail().getText().toString().trim(), login.getPassword().getText().toString().trim());
                } else {
                    Toast.makeText(LoginActivity.this, getText(R.string.emptyPassword), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(LoginActivity.this, getText(R.string.emptyLogin), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.failConection), Toast.LENGTH_LONG).show();
        }
    }

    private void signIn(final String email, String password) {
        dialogHoldon.showDialog();

        final Gson gson = new Gson();
        // [START sign_in_with_email]
        UserLogin user = new UserLogin(email, password);

        Call<ResponseLogin> call = new RetrofitConfig().getService().login(user);
        call.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {

                dialogHoldon.setCanceledOnTouchOutside(false);
                dialogHoldon.hideDialog();

                if (response.code()==200) {

                    ResponseLogin resp = response.body();

                    Headers headerList = response.headers();

                    pref.setUid(headerList.get("uid"));
                    pref.setClient(headerList.get("client"));
                    pref.setToken(headerList.get("access-token"));

                    openHome();

                } else if (response.code()==401){
                    Toast.makeText(LoginActivity.this, getString(R.string.errorLogin), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
                Log.e("TESTE FALSE> ", gson.toJson(t));
                dialogHoldon.hideDialog();
            }
        });

    }

    void openHome() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
