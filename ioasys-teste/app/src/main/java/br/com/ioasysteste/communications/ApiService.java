package br.com.ioasysteste.communications;

import br.com.ioasysteste.model.UserLogin;
import br.com.ioasysteste.model.response.ResponseEnterprise;
import br.com.ioasysteste.model.response.ResponseEnterprises;
import br.com.ioasysteste.model.response.ResponseLogin;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @POST("users/auth/sign_in")
    Call<ResponseLogin> login(@Body UserLogin user);

    @GET("/api/v1/enterprises?")
    Call<ResponseEnterprises> getEnterprises(
            @Header("Content-Type") String contentType,
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid,
            @Query("enterprise_types") String type,
            @Query("name") String name);

    @GET("/api/v1/enterprises/{id}")
    Call<ResponseEnterprise> getEnterprise(
            @Header("Content-Type") String contentType,
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid,
            @Path("id") int id);

}
