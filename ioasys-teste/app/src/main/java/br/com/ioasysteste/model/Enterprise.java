package br.com.ioasysteste.model;

import com.google.gson.annotations.SerializedName;

public class Enterprise {
    @SerializedName("id")
    int id;

    @SerializedName("email_enterprise")
    String email_enterprise;

    @SerializedName("facebook")
    String facebook;

    @SerializedName("twitter")
    String twitter;

    @SerializedName("linkedin")
    String linkedin;

    @SerializedName("phone")
    String phone;

    @SerializedName("own_enterprise")
    boolean own_enterprise;

    @SerializedName("enterprise_name")
    String enterprise_name;

    @SerializedName("photo")
    String photo;

    @SerializedName("description")
    String description;

    @SerializedName("city")
    String city;

    @SerializedName("country")
    String country;

    @SerializedName("value")
    int value;

    @SerializedName("share_price")
    float share_price;

    @SerializedName("enterprise_type")
    EnterpriseType enterpriseType;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(String email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(boolean own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public float getShare_price() {
        return share_price;
    }

    public void setShare_price(float share_price) {
        this.share_price = share_price;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }
}
