package br.com.ioasysteste.viewModel;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ioasysteste.R;

public class EnterpriseViewModel {
    ImageView imgEnterprise;
    TextView textDescription;

    public EnterpriseViewModel(Activity context) {
        imgEnterprise = (ImageView) context.findViewById(R.id.imgEnterprise);
        textDescription = (TextView) context.findViewById(R.id.textDescription);
    }

    public ImageView getImgEnterprise() {
        return imgEnterprise;
    }

    public void setImgEnterprise(ImageView imgEnterprise) {
        this.imgEnterprise = imgEnterprise;
    }

    public TextView getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(TextView textDescription) {
        this.textDescription = textDescription;
    }
}
