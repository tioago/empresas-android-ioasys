package br.com.ioasysteste.model.response;

import com.google.gson.annotations.SerializedName;

import br.com.ioasysteste.model.Enterprise;

public class ResponseEnterprise {

    @SerializedName("success")
    boolean success;

    @SerializedName("enterprise")
    Enterprise enterprise;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }
}
